#!/bin/bash

echo "Hello World!" > ./in
gzip -f -k ./in
zdiff ./in ./in.gz
result=$?
rm -f *.gz ./out ./in
exit $result

