#!/bin/bash

echo "Hello" > ./in
echo "World!" > ./in2
echo "Hello" > ./exp
echo "World!" >> ./exp
gzip -f -k ./in 
gzip -f -k ./in2
zcat ./in.gz ./in2.gz >out
diff ./exp ./out
result=$?
rm -f ./in ./in2 *.gz ./out ./exp
exit $result

