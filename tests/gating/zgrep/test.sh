#!/bin/bash

echo "Hello World!" > ./in
gzip -f -k ./in
zgrep "ello" ./in.gz >/dev/null
result=$?
rm -f *.gz ./out ./in
exit $result

