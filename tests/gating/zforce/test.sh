#!/bin/bash

echo "Hello World!" > ./in
gzip -f -c ./in > ./out
zforce ./out > /dev/null
ls ./out.gz >/dev/null
result=$?
rm -f ./in *.gz
exit $result

